uniform float seperation;

uniform sampler2D textureLeft;
uniform sampler2D textureRight;

void main()
{
// Compute the texture coordinates based on the seperation
    vec2 texCoord = gl_TexCoord[0].xy;

    vec3 outFrag = vec3(0.);
    
// Left image coordinates
    vec2  leftCoords = vec2(texCoord.x + 1. - seperation,
                            texCoord.y);
    
    if (leftCoords.x >= 0. &&
        leftCoords.x <= 1.) {

        // Lookup the pixel for the fragment from the images
        vec3  leftTexel = texture2D(textureLeft,   leftCoords).rgb;
        
        outFrag.r = leftTexel.r;
    }
    
// Right image coordinates
    vec2 rightCoords = vec2(texCoord.x + seperation,
                            texCoord.y);

    if (rightCoords.x >= 0. &&
        rightCoords.x <= 1.) {

        // Lookup the pixel for the fragment from the images
        vec3 rightTexel = texture2D(textureRight, rightCoords).rgb;
        
        outFrag.gb = rightTexel.gb;
    }
    
	gl_FragColor = vec4(outFrag, 1.);
}
