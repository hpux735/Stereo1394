//
//  DCAMAppDelegate.h
//  Stereo 1394
//
//  Created by William Dillon on 7/17/12.
//  Copyright (c) 2012 Oregon State Universiry (CEOAS). All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class DCAMDevice;
@class DCAMFrameView;

@interface DCAMAppDelegate : NSObject <NSApplicationDelegate>
{
    NSArray *deviceList;
    int _selectedDevice;

    DCAMDevice *device;
    DCAMDevice *deviceTwo;
    
    float gain;
    float exposure;
    float shutter;
    float focus;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet DCAMFrameView *frameView;

@property (readonly)  NSArray *deviceList;
@property (readwrite) int selectedDevice;

@property (readwrite) float gain;
@property (readwrite) float exposure;
@property (readwrite) float shutter;
@property (readwrite) float focus;

@property (readwrite) float frameSep;
@property (readwrite) bool anaglyph;
@property (readwrite) bool reverse;

@property (assign) IBOutlet NSSlider *gainSlider;
@property (assign) IBOutlet NSSlider *exposureSlider;
@property (assign) IBOutlet NSSlider *shutterSlider;
@property (assign) IBOutlet NSSlider *focusSlider;
@property (assign) IBOutlet NSSlider *frameSlider;

- (IBAction)printFeatures:(id)sender;

- (IBAction)reverseCheckBox:(id)sender;
- (IBAction)anaglyphCheckBox:(id)sender;

@end
