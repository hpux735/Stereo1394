//
//  main.m
//  Stereo 1394
//
//  Created by William Dillon on 7/17/12.
//  Copyright (c) 2012 Oregon State Universiry (CEOAS). All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
