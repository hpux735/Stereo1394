//
//  DCAMFrameView.h
//  Basic lib1394
//
//  Created by William Dillon on 7/9/12.
//  Copyright (c) 2012 William Dillon. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OpenGLController.h"
#import "DCAMAppDelegate.h"
#import "ShaderProgram.h"

//#define TEXTURE_TYPE GL_TEXTURE_RECTANGLE_ARB
#define TEXTURE_TYPE GL_TEXTURE_2D

@interface DCAMFrameView : OpenGLController
{
    bool initialized;
    
	float _frameSeperation;
    
    // These ivars maintain OpenGL state
	bool textureCurrent;
	unsigned int  leftTextureID;
	unsigned int rightTextureID;

    NSData  *leftFrameData;
    NSData *rightFrameData;
    
    ShaderProgram *shader;
    
    int _width;
    int _height;

    bool anaglyph;
    id leftDevice;
    id rightDevice;
    
    NSData *newSlice;    
}

@property (readwrite) IBOutlet DCAMAppDelegate *appDelegate;

@property (readwrite) int width;
@property (readwrite) int height;

@property (readwrite) float frameSeperation;

@property (readonly) unsigned int textureID;

@property (assign) id leftDevice;
@property (assign) id rightDevice;

- (void)initialize;
- (void)frameNotification:(NSNotificationCenter *)notification;

@end
