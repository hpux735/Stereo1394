//
//  DCAMFrameView.m
//  Basic lib1394
//
//  Created by William Dillon on 7/9/12.
//  Copyright (c) 2012 William Dillon. All rights reserved.
//

#import "DCAMDevice.h"
#import "DCAMFrameView.h"
#import "OpenGLView.h"

@implementation DCAMFrameView

#pragma mark -
#pragma mark Init and bookkeeping methods
+ (NSOpenGLPixelFormat *)defaultPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFAAccumSize, 32,
        NSOpenGLPFADepthSize, 16,
        NSOpenGLPFAMultisample,
        NSOpenGLPFASampleBuffers, (NSOpenGLPixelFormatAttribute)1,
        NSOpenGLPFASamples, (NSOpenGLPixelFormatAttribute)4,
        (NSOpenGLPixelFormatAttribute)nil };
    
    return [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];
}

- (void)awakeFromNib
{
	[super awakeFromNib];
    
	textureCurrent = NO;
    
    // Subscribe to frame notifications
    NSNotificationCenter *center;
    center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(frameNotification:)
                   name:DCAMCapturedFrameNotification object:nil];
}

- (void)initGL
{
    initialized = NO;
    return;
}

-(void)initialize
{
    // Clear any errors
    GLint error = glGetError();

    if (initialized) {
        return;
    } else {
        initialized = YES;
    }
    
    // Set black background
	glClearColor(0., 0., 0., 1.);
	
    // Set viewing mode
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, 1., 0., 1., -1., 1.);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    
    // Set blending characteristics
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    // Set line width
	glLineWidth( 1.5 );
	
	glDisable(GL_DEPTH_TEST );
    glEnable( GL_TEXTURE_2D );
    
    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }
    
    // Create the shader program
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *fragURL = [bundle URLForResource:@"fragShader" withExtension:@"frag"];
    
    NSError *nsError = nil;
    NSString *fragString = [NSString stringWithContentsOfURL:fragURL encoding:NSUTF8StringEncoding error:&nsError];
    if (fragString == nil) {
        if (nsError != nil) {
            NSLog(@"Unable to open fragment file: %@", [nsError localizedDescription]);
        }
        
        return;
    }

    shader = [[ShaderProgram alloc] initWithVertex:nil andFragment:fragString];
    if (shader == nil) {
        NSLog(@"Unable to create shader program!");
        return;
    }
    
    unsigned char *blankImage = malloc(4 * _width * _height * sizeof(float));
    //    bzero(blankImage, sizeof(blankImage));
    for (int i = 0; i < _height; i++) {
        for (int j = 0; j < _width; j++) {
            // Color cube
            blankImage[i*_width*3 + j*3 + 0] = ((i % 10) < 5)? 1. : 0.;
            blankImage[i*_width*3 + j*3 + 1] = ((j % 10) < 5)? 1. : 0.;
            blankImage[i*_width*3 + j*3 + 2] = 0.;
            blankImage[i*_width*3 + j*3 + 2] = 1.;
        }
    }
    
    // Setup textures
	glGenTextures( 1, (GLuint*)&leftTextureID );
	glBindTexture(  TEXTURE_TYPE, leftTextureID );
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(TEXTURE_TYPE, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_FLOAT, blankImage);
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );

	glGenTextures( 1, (GLuint*)&rightTextureID );
    glBindTexture(  TEXTURE_TYPE, rightTextureID );
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(TEXTURE_TYPE, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_FLOAT, blankImage);
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );

    // Setup the shader uniforms
    glActiveTexture(GL_TEXTURE0);
    glBindTexture( TEXTURE_TYPE, leftTextureID );
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture( TEXTURE_TYPE, rightTextureID );

    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }

    free(blankImage);

	glBindTexture(  TEXTURE_TYPE, 0 );
}

- (void)frameNotification:(NSNotification *)notification
{
    NSDictionary *userDict = [notification userInfo];
    
    id sender = userDict[@"device"];
    
    if (sender == self.leftDevice) {
        leftFrameData = (NSData *)[notification object];
    }
    
    if (sender == self.rightDevice) {
        rightFrameData = (NSData *)[notification object];
    }
    
    // Redraw the display
    textureCurrent = NO;
    [super updateData:self];
}

- (void)draw
{
    // Clear any errors
    GLint error = glGetError();
   
    glClearColor(0., 0., 0., 1.);
    glClear(GL_COLOR_BUFFER_BIT);
    
    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }

    if (!textureCurrent) {
        if (leftFrameData != nil) {
            glBindTexture( TEXTURE_TYPE, leftTextureID );
            
            const unsigned char *image = [leftFrameData bytes];
            glTexImage2D(TEXTURE_TYPE, 0, GL_RGB,
                         self.width, self.height, 0,
                         GL_RGB, GL_UNSIGNED_BYTE, image);
            leftFrameData = nil;
        }
        
        if (rightFrameData != nil) {
            glBindTexture( TEXTURE_TYPE, rightTextureID );

            const unsigned char *image = [rightFrameData bytes];
            glTexImage2D(TEXTURE_TYPE, 0, GL_RGB,
                         self.width, self.height, 0,
                         GL_RGB, GL_UNSIGNED_BYTE, image);
            rightFrameData = nil;
        }
    }

    glBindTexture(TEXTURE_TYPE, 0);
    
    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }

    [shader bind];
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture( TEXTURE_TYPE, rightTextureID );
    [shader setIntValue:1 forUniform:@"textureRight"];

    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture( TEXTURE_TYPE, leftTextureID );
    [shader setIntValue:0 forUniform:@"textureLeft"];
    
    
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }
    
    [shader setFloatValue:self.frameSeperation forUniform:@"seperation"];
    
    glBegin( GL_QUADS ); {
		glColor4f( 0., 1., 0., 1. );
        
        glTexCoord2d(-1., 1.);
		glVertex2f(  -1., 0.);
        
		glTexCoord2d(-1., 0.);
		glVertex2f(  -1., 1.);
        
		glTexCoord2d( 1., 0.);
		glVertex2f(   1., 1.);
        
		glTexCoord2d( 1., 1.);
		glVertex2f(   1., 0.);
	} glEnd();

    [shader unBind];
    
    glFlush();
}

- (float)frameSeperation { return _frameSeperation; }
- (void)setFrameSeperation:(float)frameSeperation
{
    _frameSeperation = frameSeperation;
    [super updateData:self];
}

@end













































